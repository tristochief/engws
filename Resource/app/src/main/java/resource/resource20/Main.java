package resource.resource20;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import java.sql.*;

public class Main extends AppCompatActivity {
    //?user=vatdflujsvqwov&password=895732bb71813033c4f00dc0fad9d9561b3a0815d2571de2b225e6eada37034f&sslmode=require&PORT=44082
    static String url = "jdbc:postgresql://ec2-50-19-83-146.compute-1.amazonaws.com:5432/dcbs3ce1nh6bpi";
    static String username = "vatdflujsvqwov";
    static String password = "895732bb71813033c4f00dc0fad9d9561b3a0815d2571de2b225e6eada37034f";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("bla", "hello?");
    }

    public void hasResources(View view) {
        Intent intent = new Intent(this, ResourceSender.class);
        startActivity(intent);
    }

    public void hasNoResources(View view) {
        Log.d("database", "before");
        try {
            Class.forName("org.postgresql.Driver");

            Connection conn = DriverManager.getConnection(url, username, password);
            Statement stmnt = conn.createStatement();

            String sql = "SELECT * FROM Example";

            ResultSet resultSet = stmnt.executeQuery(sql);

            Log.d("", "did connect successfully");
        } catch(SQLException e) {
            Log.d("error db: sqlException ",e.getMessage());
        } catch(ClassNotFoundException e) {
            Log.d("error db: classNotFound", e.getMessage());
        }

        Log.d("database", "success");

        Intent intent = new Intent(this, ResourceSelect.class);
        startActivity(intent);
    }
}
